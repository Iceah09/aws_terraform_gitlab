import os
import boto3
import json


def handler(event, context):
    print('Welcome')
    s3 = boto3.resource(
    's3'
    )
    content="Bonjour, merci d'avoir lu ce fichier !"
    print('Upload/Override to s3')
    s3.Object('itw-s3-bucket', 'hello.txt').put(Body=content)

    print('Read from s3')

    bucket = s3.Bucket('itw-s3-bucket')

    for obj in bucket.objects.all():
        key = obj.key
        print(obj.get()['Body'].read())


    print('Lambda éxecutée. Vous devriez vérifier les fichiers du bucket !')



