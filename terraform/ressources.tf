
# ecr
resource "aws_ecr_repository" "test" {
  name         = "ecr-repo-dev"

  image_scanning_configuration {
    scan_on_push = true
  }
}