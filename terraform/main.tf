# Fargate node groups example

terraform {
  required_version = "~> 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

output "ecr" {
  value = "${aws_ecr_repository.test.repository_url}"
}
