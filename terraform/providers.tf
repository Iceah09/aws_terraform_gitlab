provider "aws" {
  region = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
provider "docker" {
  host = "unix:///var/run/docker.sock"
}