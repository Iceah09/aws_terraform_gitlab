# bucker s3
module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "itw-s3-bucket"
  acl    = "private"

  versioning = {
    enabled = true
  }

}
/*

module "lambda-cloudwatch-trigger" {
  source  = "infrablocks/lambda-cloudwatch-events-trigger/aws"
  region                = "eu-west-3"
  component             = "python-docker-dev-hello"
  deployment_identifier = "production"

  lambda_arn =  "arn:aws:lambda:eu-west-3:973311967478:function:python-docker-dev-hello"
  lambda_function_name = "python-docker-dev-hello"
  lambda_schedule_expression = "rate(2 minutes)"
}
*/
