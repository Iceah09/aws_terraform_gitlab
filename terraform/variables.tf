# Variables for providing to module fixture codes

### network
variable "aws_region" {
  description = "The aws region to deploy"
  default = "eu-west-3"
  type        = string
}
### aws credentials
variable "aws_access_key"{
  description = "The access key"
  type        = string
}
variable "aws_secret_key"{
  description = "The secret key"
  type        = string
}