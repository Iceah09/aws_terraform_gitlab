aiohttp==3.7.4.post0
aioitertools==0.8.0
alabaster==0.7.12
async-timeout==3.0.1
attrs==21.2.0
Babel==2.9.1
boto3==1.18.43
botocore==1.21.44
certifi==2021.5.30
chardet==4.0.0
charset-normalizer==2.0.5
click==8.0.1
docutils==0.16
Flask==2.0.1
fsspec==2021.8.1
futures==3.1.1
idna==3.2
imagesize==1.2.0
itsdangerous==2.0.1
Jinja2==3.0.1
jmespath==0.10.0
MarkupSafe==2.0.1
multidict==5.1.0
packaging==21.0
Pygments==2.10.0
pyparsing==2.4.7
python-dateutil==2.8.2
pytz==2021.1
requests==2.26.0
s3transfer==0.5.0
six==1.16.0
snowballstemmer==2.1.0
Sphinx==4.2.0
sphinx-rtd-theme==0.5.2
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==2.0.0
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.5
typing-extensions==3.10.0.2
urllib3==1.26.6
Werkzeug==2.0.1
wrapt==1.12.1
xmltodict==0.12.0
yarl==1.6.3
